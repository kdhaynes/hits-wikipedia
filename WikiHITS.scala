/*
   ***************************************************************
   Hyperlink-Induced Topic Search (HITS) Over Wikipedia Articles
   Using Apache Spark
   Katherine Haynes, CS535
   February 18, 2020

   This program generates a list of the top 50 authority and hub pages
   on a user-supplied word.

   To open the spark server information:
   >xdg-open http://indianapolis.cs.colostate.edu:31471

   To open the hadoop cluster information:
   >xdg-open http://indianapolis.cs.colostate.edu:31451

   To see the results:
   >mhadoop fs -ls /output/

   To compile this program:
   >sbt package
   
   To run this program on the cluster locally:
   >spark-submit --class WikiHITS --master local --supervise target/scala-2.11/wikihits_2.11-1.0.jar

   To run this program on the cluster distributed:
   >spark-submit --class WikiHITS --deploy-mode cluster --master spark://indianapolis:31470 --supervise target/scala-2.11/wikihits_2.11-1.0.jar eclipse

   The output can be written just to the standard output or a file.
   To write to a file, set the write flag to true and supply a filename.
      - For the root set, change writeRoot and fileRoot
      - For the base set, change writeBase and fileBase
      - For the authority values, change writeAuth and fileAuth
      - For the hub values, change writeHub and fileHub

   This approach is iterative, and the iteration cycle
   is stopped the first time one of the following occurs:
     - Maximum number of iterations is reached
        (specified by countMax parameter)
     - Threshold for both authority and hub differences
        is met (specified by ahThresh parameter)

   ***************************************************************
*/

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.compress.CompressionCodec
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import scala.math.sqrt

object WikiHITS {

  //**** File Names For Titles and Links ***//
  val fileTitles = "hdfs://indianapolis:31460/input/titles-sorted.txt"
  val fileLinks = "hdfs://indianapolis:31460/input/links-simple-sorted.txt"
  //val fileTitles = "hdfs://indianapolis:31460/input/simpleTitles.txt"
  //val fileLinks = "hdfs://indianapolis:31460/input/simpleLinks.txt"

  var countMax = 100
  var ahThresh = 1.0E-10

  val writeRoot = false
  val fileRoot = "hdfs://indianapolis:31460/output/wikiRoot"

  val writeBase = false
  val fileBase = "hdfs://indianapolis:31460/output/wikiBase"

  val writeAuth = false
  val fileAuth = "hdfs://indianapolis:31460/output/wikiAuth"

  val writeHub = false
  val fileHub  = "hdfs://indianapolis:31460/output/wikiHub"
  
  // Main Program
  def main(args: Array[String]) {
    //println("Hello!")
    if (args.length == 0) {
       println("Please specify a topic query.")
       System.exit(0)
    }
    val wikiKey = args(0).toLowerCase()
    println("")
    println("Searching For: " + wikiKey)

    // Start Spark Session
    val spark = SparkSession.builder.appName("WikiHITS").getOrCreate()
    val sc = spark.sparkContext

    // Open the data
    val wikiLinks = spark.read.textFile(fileLinks).rdd
                    .map(row => row.split(":"))
		    .map(kv => (kv(0).toInt, kv(1).trim().split("\\s").map(_.toInt)))
		    .flatMapValues(x => x)
    //wikiLinks.take(10).foreach(println)
    
    val wikiTitles = spark.read.textFile(fileTitles)
                    .rdd.zipWithIndex
                    .mapValues(line => (line+1).toInt)
		    .map(_.swap)
		    .mapValues(str => str.toLowerCase())
    //wikiTitles.take(10).foreach(println)

    // Get the Root Set
    val rootSet = wikiTitles.filter{case (line,title) => title.contains(wikiKey)}
    println("Root Set (Line, Title): ")
    println("  Number of Elements: %s".format(rootSet.count()))
    println("  First 10 Sample:")
    rootSet.take(10).foreach(println)

    val outputfile = "/output/temp"
    var filename = "test"
    var outputFileName = outputfile + "/temp_" + filename
    var mergedFileName = outputfile + "/merged_" + filename
    var mergeFindGlob = outputFileName
    if (writeRoot) {
        val conf = sc.hadoopConfiguration
	val fs = org.apache.hadoop.fs.FileSystem.get(conf)
	val exists = fs.exists(new org.apache.hadoop.fs.Path(fileRoot))
	if (exists) {
	    println("File Exists, Not Overwriting!")
	} else {
            println("Writing File: %s".format(fileRoot))
            rootSet.coalesce(1).saveAsTextFile(fileRoot)
	}
    }
    println("")

    // Get the Base Set
    val cond2 = wikiLinks.join(rootSet).mapValues(x => x._1).values
    val cond3 = wikiLinks.map(_.swap).join(rootSet).mapValues(x => x._1).values
    val baseSet = rootSet.keys.union(cond2).union(cond3).map(x => (x,""))
         .join(wikiTitles).mapValues(x => x._2).distinct
    println("Base Set (Line, Title): ")
    println("  Number of Elements: %s".format(baseSet.count()))
    println("  First 10 Sample:")
    baseSet.take(10).foreach(println)

    if (writeBase) {
        val conf = sc.hadoopConfiguration
	val fs = org.apache.hadoop.fs.FileSystem.get(conf)
	val exists = fs.exists(new org.apache.hadoop.fs.Path(fileBase))
	if (exists) {
	    println("Base File Exists, Not Overwriting!")
	} else {
	    println("Writing File: %s".format(fileBase))
	    baseSet.coalesce(1).saveAsTextFile(fileBase)
	}
    }
    println("")

    // Get the list of what each base points to
    val fromBaseSet = wikiLinks.join(baseSet).mapValues(x => x._1)
                       .map(_.swap).join(baseSet).mapValues(x => x._1).map(_.swap)
    //println("From Base: ")
    //fromBaseSet.take(10).foreach(println)
    //println("")

    // Get the list of what points to each base
    val toBaseSet = wikiLinks.map(_.swap).join(baseSet).mapValues(x => x._1)
                    .map(_.swap).join(baseSet).mapValues(x => x._1).map(_.swap)
    //println("To Base: ")
    //toBaseSet.take(10).foreach(println)
    //println("")
    
    // Find the authority/hub scores
    var aResult = baseSet.mapValues(str => 1.0)
    var hResult = baseSet.mapValues(str => 1.0)
    var aDiffAcc = sc.accumulator(1.0)
    var hDiffAcc = sc.accumulator(1.0)
    var count = 0
    while ((count < countMax) &&
           ((aDiffAcc.value > ahThresh) || (hDiffAcc.value > ahThresh))){
        count += 1
	
        //println("**Iteration: %s**".format(i+1))
        val aTemp = toBaseSet.map(_.swap).join(hResult).map{case (x,(y,z)) => (y,z)}
        val aIter = aTemp.reduceByKey((accum,n) => (accum + n))
        var aTot = sc.accumulator(0.0)
        aIter.foreach{case (x,y) => aTot.add(y)}
        var accVal = aTot.value
        val aResultNew = aIter.mapValues(x => x.toDouble/accVal)
	//println("Authority Values (Line, Value): ")
        //aResultNew.collect().foreach(println)
	//println("")

        val hTemp = fromBaseSet.map(_.swap).join(aResultNew).map{case (x,(y,z)) => (y,z)}
	val hIter = hTemp.reduceByKey((accum,n) => (accum + n))
	var hTot = sc.accumulator(0.0)
	hIter.foreach{case (x,y) => hTot.add(y)}
	var hccVal = hTot.value
	val hResultNew = hIter.mapValues(x => x.toDouble/hccVal)
	//println("Hub Values (Line, Value): ")
	//hResult.collect().foreach(println)
        //println("")

        //Check for convergence
	aDiffAcc.setValue(0.0)
	hDiffAcc.setValue(0.0)
	val aDiff = aResult.join(aResultNew).map{case (x,(y,z)) => (x,(y-z)*(y-z).toDouble)}
	val hDiff = hResult.join(hResultNew).map{case (x,(y,z)) => (x,(y-z)*(y-z).toDouble)}
	
	aDiff.foreach{case (x,y) => aDiffAcc.add(y)}
	hDiff.foreach{case (x,y) => hDiffAcc.add(y)}
	aDiffAcc.setValue(sqrt(aDiffAcc.value))
	hDiffAcc.setValue(sqrt(hDiffAcc.value))
	aResult = aResultNew
	hResult = hResultNew
     }

    println("Number of Iterations: %s".format(count))
    println("Authority Change: %s".format(aDiffAcc.value))
    println("Hub Change: %s".format(hDiffAcc.value))
    println("")
    
    println("Top Authority Values (Score, Page Title): ")
    val aPresent = aResult.join(wikiTitles).map{case (x,(y,z)) => (y,z)}
    aPresent.sortByKey(false).take(50).foreach(println)

    if (writeAuth) {
        val conf = sc.hadoopConfiguration
	val fs = org.apache.hadoop.fs.FileSystem.get(conf)
	val existsA = fs.exists(new org.apache.hadoop.fs.Path(fileAuth))
	if (existsA) {
	    println("Authority File Exists, Not Overwriting!")
	} else {
	    println("Writing File: %s".format(fileAuth))
	    sc.parallelize(aPresent.sortByKey(false).take(50)).coalesce(1).saveAsTextFile(fileAuth)
	}
    }
    println("")
    
    println("Top Hub Values (Score, Page Title): ")
    val hPresent = hResult.join(wikiTitles).map{case (x, (y,z)) => (y,z)}
    hPresent.sortByKey(false).take(50).foreach(println)

    if (writeHub) {
       val conf = sc.hadoopConfiguration
       val fs = org.apache.hadoop.fs.FileSystem.get(conf)
       val existsH = fs.exists(new org.apache.hadoop.fs.Path(fileHub))
       if (existsH) {
           println("Hub File Exists, Not Overwriting!")
       } else {
           println("Writing File: %s".format(fileHub))
	   sc.parallelize(hPresent.sortByKey(false).take(50)).coalesce(1).saveAsTextFile(fileHub)
      } 
    }
    println("")
    
    spark.stop()
  }

}